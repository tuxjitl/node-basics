const fs = require("fs");

const requestHandler = (req, res) => {
  const url = req.url;
  const method = req.method;

  if (url === "/") {
    res.write("<html>");
    res.write("<head><title>Enter message</title></head>");
    res.write("<body>");
    res.write("<form action='/message' method='POST'>");
    res.write(
      "<input type='text' name='message' placeholder='Enter your message'>"
    );

    res.write("<button type='submit'>Submit</button>");
    res.write("</form>");
    res.write("</body>");
    res.write("</html>");
    return res.end();
  }
  if (url === "/message" && method === "POST") {
    const body = []; //empty array
    req.on("data", (chunk) => {
      body.push(chunk);
    });

    return req.on("end", () => {
      const parsedBody = Buffer.concat(body).toString(); //stringefy chuncks (hexadecimal)
      const message = parsedBody.split("=")[1]; //split on = then take index = 1
      //create file with user send data
      fs.writeFile("message.txt", message, (err) => {
        //redirect to another page in 2 steps, can be done in 1 with sethead
        res.statusCode = 302; //integer ==> code voor redirect
        res.setHeader("Location", "/"); //redirect location to root
        return res.end();
      });
    });
  }
  res.setHeader("content-type", "text/html");

  res.write("<html>");
  res.write("<head><title>My first page</title></head>");
  res.write("<body>");
  res.write("<h1>");
  res.write("Hello world");
  res.write("</h1>");
  res.write("</body>");
  res.write("</html>");
  res.end();
};

module.exports = requestHandler;
