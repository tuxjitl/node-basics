const http = require("http");

const routes = require("./routes");

/**
 * createServer: Maak een http server, returns a server ==> opvangen in een constant
 *
 * Parameters: Gebruik van een arrow function, dit is een callback function
 *
 * @param req - contains data from request.
 * @param res - contains the response.
 *
 *
 * @return void
 *
 *
 */
const server = http.createServer(routes);

/**
 * NodeJS stopt het script niet onmiddelijk, maar luistert naar requests.
 *
 * @param port De poort waar je op luistert
 */
server.listen(30000);

